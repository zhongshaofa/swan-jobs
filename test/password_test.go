package test

import (
	"github.com/zhongshaofa/swan-jobs/internal/utils/password"
	"testing"
)

func TestPassword(t *testing.T) {
	pwd := password.EncryptPassword("admin")
	if pwd != "e46d6f876514ec1fb9e246331d5a2c2b" {
		t.Error("密码加密规则异常")
	}
}
