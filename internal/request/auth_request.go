package request

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/zhongshaofa/swan-jobs/internal/models"
)

type AuthRequest struct {
	UserId   int
	RoleType int
}

func GetAuthRequest(gc *gin.Context) (*AuthRequest, error) {
	userId, exists1 := gc.Get(models.CtxUserId)
	roleType, exists2 := gc.Get(models.CtxRoleType)

	if !exists1 || !exists2 {
		return nil, errors.New("登录信息异常，请重新登录")
	}

	return &AuthRequest{
		UserId:   userId.(int),
		RoleType: roleType.(int),
	}, nil
}
