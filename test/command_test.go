package test

import (
	"github.com/zhongshaofa/swan-jobs/internal/services"
	"testing"
)

func TestCommand(t *testing.T) {
	go services.ClientChanListening()
	go testGo()
	select {}
}

func testGo()  {
	parameter := services.ClientParameter{
		Directory:  "Directory",
		Command:    "Command",
		Timeout:    1,
		TaskId:     2,
		ScheduleId: 3,
		Mode:       4,
	}
	go func() {
		services.ParameterChan <- parameter
	}()
}
