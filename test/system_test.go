package test

import (
	"github.com/zhongshaofa/swan-jobs/internal/utils/system"
	"testing"
)

func TestSystemGetCpuPercent(t *testing.T) {
	test := int32(system.GetUsedCpuPercent())
	if test > 0 && test < 100 {
		t.Log(test)
	} else {
		t.Error("GetUsedCpuPercent error")
	}
}

func TestSystemGetIdleCpuPercent(t *testing.T) {
	test := int32(system.GetIdleCpuPercent())
	if test > 0 && test < 100 {
		t.Log(test)
	} else {
		t.Error("GetIdleCpuPercent error")
	}
}

func TestSystemGetMemoryPercent(t *testing.T) {
	test := int32(system.GetUsedMemoryPercent())
	if test > 0 && test < 100 {
		t.Log(test)
	} else {
		t.Error("GetUsedMemoryPercent error")
	}
}

func TestSystemGetIdleMemoryPercent(t *testing.T) {
	test := int32(system.GetIdleMemoryPercent())
	if test > 0 && test < 100 {
		t.Log(test)
	} else {
		t.Error("GetIdleMemoryPercent error")
	}
}
