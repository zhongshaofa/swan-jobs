package test

import (
	"fmt"
	"github.com/zhongshaofa/swan-jobs/internal/models"
	"github.com/zhongshaofa/swan-jobs/internal/repositorys"
	"github.com/zhongshaofa/swan-jobs/internal/request"
	"github.com/zhongshaofa/swan-jobs/test/tool"
	"testing"
)

func TestTaskRepositoryGetList(t *testing.T) {
	tool.InitDB()
	repository := repositorys.TaskRepository{}

	list, _, err := repository.GetList(&models.Task{Status: models.StatusEnable}, request.PaginationRequest{
		Page:  2,
		Limit: 100,
	})
	fmt.Println(err)
	fmt.Printf("%v\n", list)
}
