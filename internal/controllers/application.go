package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/zhongshaofa/swan-jobs/internal/models"
	"github.com/zhongshaofa/swan-jobs/internal/repositorys"
	"github.com/zhongshaofa/swan-jobs/internal/request"
	"github.com/zhongshaofa/swan-jobs/internal/resource"
	"github.com/zhongshaofa/swan-jobs/internal/utils/response"
	"github.com/zhongshaofa/swan-jobs/internal/utils/validator"
	"strconv"
)

type application struct {
	applicationRepository repositorys.ApplicationRepository
}

var Application = application{}

func (c *application) List(gc *gin.Context) {
	p := request.GetPaginationRequest(gc)
	m := models.Application{}

	appName, _ := gc.GetQuery("app_name")
	appCode, _ := gc.GetQuery("app_code")
	status, _ := gc.GetQuery("status")
	if len(appName) > 0 {
		m.AppName = appName
	}
	if len(appCode) > 0 {
		m.AppCode = appCode
	}
	if len(status) > 0 {
		statusInt, _ := strconv.Atoi(status)
		m.Status = statusInt
	}

	list, count, _ := c.applicationRepository.GetList(&m, p)
	response.RespListSuccess(list, count, p, gc)
	return
}

func (c *application) Add(gc *gin.Context) {
	var r request.ApplicationAddRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	_, err = c.applicationRepository.Create(&models.Application{
		AppName:      r.AppName,
		AppCode:      r.AppCode,
		Introduction: r.Introduction,
		Remark:       r.Remark,
		Status:       r.Status,
	})
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *application) Edit(gc *gin.Context) {
	var r request.ApplicationEditRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	m, err := c.applicationRepository.Detail(r.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}

	m.AppName = r.AppName
	m.AppCode = r.AppCode
	m.Introduction = r.Introduction
	m.Remark = r.Remark
	m.Status = r.Status

	err = c.applicationRepository.Update(m)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess("", gc)
	return
}

func (c *application) Detail(gc *gin.Context) {
	idQueryRequest := request.GetIdQueryRequest(gc)
	if idQueryRequest == nil {
		response.RespError("id不能为空", gc)
		return
	}

	detail, err := c.applicationRepository.Detail(idQueryRequest.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(detail, gc)
	return
}

func (c *application) Delete(gc *gin.Context) {
	var r request.IdsFormRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	err = c.applicationRepository.Delete(r.Ids)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess("", gc)
	return
}

func (c *application) SwitchStatus(gc *gin.Context) {
	var r request.SwitchStatusFormRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	err = c.applicationRepository.SwitchStatus(r.Ids, r.Status)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *application) ClientAuth(gc *gin.Context) {
	var r request.ApplicationClientAuthRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	err = c.applicationRepository.ClientAuth(r.Id, r.ClientIds)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *application) ClientList(gc *gin.Context) {
	idQueryRequest := request.GetIdQueryRequest(gc)
	if idQueryRequest == nil {
		response.RespError("id不能为空", gc)
		return
	}

	detail, err := c.applicationRepository.ClientList(idQueryRequest.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}

	result := resource.ApplicationClientListResourceCreate(*detail)

	response.RespSuccess(result, gc)
	return
}
