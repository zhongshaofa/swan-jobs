package utils

import (
	"io/ioutil"
	"net/http"
)

// HttpGet http get 请求
func HttpGet(url string) (error, string) {
	resp, err := http.Get(url)
	if err != nil {
		return err, ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err, ""
	}
	return nil, string(body)
}
