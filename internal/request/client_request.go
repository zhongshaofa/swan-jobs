package request

type ClientAddRequest struct {
	Ip         string `form:"ip" json:"ip" binding:"required"`
	Port       int    `form:"port" json:"port" binding:"required"`
	ClientName string `form:"client_name" json:"client_name" binding:"required"`
	ClientCode string `form:"client_code" json:"client_code" binding:"required"`
	Status     int    `form:"status" json:"status" binding:"required"`
	Remark     string `form:"remark" json:"remark"`
}

type ClientEditRequest struct {
	ClientAddRequest
	IdFormRequest
}
