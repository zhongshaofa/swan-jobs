package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/zhongshaofa/swan-jobs/internal/models"
	"github.com/zhongshaofa/swan-jobs/internal/repositorys"
	"github.com/zhongshaofa/swan-jobs/internal/request"
	"github.com/zhongshaofa/swan-jobs/internal/resource"
	"github.com/zhongshaofa/swan-jobs/internal/utils/password"
	"github.com/zhongshaofa/swan-jobs/internal/utils/response"
	"github.com/zhongshaofa/swan-jobs/internal/utils/validator"
	"strconv"
)

type user struct {
	UserRepository repositorys.UserRepository
}

var User = user{}

func (c *user) List(gc *gin.Context) {
	p := request.GetPaginationRequest(gc)
	u := models.User{}

	username, _ := gc.GetQuery("username")
	status, _ := gc.GetQuery("status")
	roleType, _ := gc.GetQuery("role_type")
	if len(username) > 0 {
		u.Username = username
	}
	if len(status) > 0 {
		statusInt, _ := strconv.Atoi(status)
		u.Status = statusInt
	}
	if len(roleType) > 0 {
		roleTypeInt, _ := strconv.Atoi(roleType)
		u.RoleType = roleTypeInt
	}

	list, count, _ := c.UserRepository.GetList(&u, p)
	response.RespListSuccess(list, count, p, gc)
	return
}

func (c *user) Add(gc *gin.Context) {
	var r request.UserAddRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	_, err = c.UserRepository.Create(&models.User{
		Username: r.Username,
		Password: password.EncryptPassword(r.Password),
		Status:   r.Status,
		RoleType: r.RoleType,
		Remark:   r.Remark,
	})
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *user) Edit(gc *gin.Context) {
	var r request.UserEditRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	u, err := c.UserRepository.Detail(r.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	u.Status = r.Status
	u.RoleType = r.RoleType
	u.Remark = r.Remark

	err = c.UserRepository.Update(u)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess("", gc)
	return
}

func (c *user) Detail(gc *gin.Context) {
	idQueryRequest := request.GetIdQueryRequest(gc)
	if idQueryRequest == nil {
		response.RespError("id不能为空", gc)
		return
	}

	detail, err := c.UserRepository.Detail(idQueryRequest.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(detail, gc)
	return
}

func (c *user) Delete(gc *gin.Context) {
	var r request.IdsFormRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	err = c.UserRepository.Delete(r.Ids)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *user) ResetPassword(gc *gin.Context) {
	var r request.UserRestartPasswordRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	if r.Password != r.AgainPassword {
		response.RespValidateError("两次密码输入不一致", gc)
		return
	}

	u, err := c.UserRepository.Detail(r.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}

	u.Password = password.EncryptPassword(r.Password)

	err = c.UserRepository.Update(u)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess("", gc)
	return
}

func (c *user) SwitchStatus(gc *gin.Context) {
	var r request.SwitchStatusFormRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	err = c.UserRepository.SwitchStatus(r.Ids, r.Status)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *user) ApplicationAuth(gc *gin.Context) {
	var r request.UserApplicationAuthRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	err = c.UserRepository.ApplicationAuth(r.Id, r.ApplicationIds)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *user) ApplicationList(gc *gin.Context) {
	idQueryRequest := request.GetIdQueryRequest(gc)
	if idQueryRequest == nil {
		response.RespError("id不能为空", gc)
		return
	}

	detail, err := c.UserRepository.ApplicationList(idQueryRequest.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}

	result := resource.UserApplicationListResourceCreate(*detail)

	response.RespSuccess(result, gc)
	return
}
