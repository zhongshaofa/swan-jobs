package models

import (
	"github.com/zhongshaofa/swan-jobs/configs"
)

type Notify struct {
	Base
	Name   string `gorm:"column:name;NOT NULL" json:"name"`
	Type   int    `gorm:"column:type;default:1;NOT NULL" json:"type"` // 通知类型 1=邮件通知 2=短信通知 3=微信通知 4=钉钉通知 5=自定义回调
	Email  string `gorm:"column:email;NOT NULL" json:"email"`
	Phone  string `gorm:"column:phone;NOT NULL" json:"phone"`
	Remark string `gorm:"column:remark;NOT NULL" json:"remark"` // 备注信息
}

func (m *Notify) TableName() string {
	return configs.DatabaseConfig.Prefix + "user_notify"
}
