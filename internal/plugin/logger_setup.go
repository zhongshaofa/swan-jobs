package plugin

import (
	"fmt"
	"github.com/zhongshaofa/swan-jobs/configs"
	"github.com/zhongshaofa/swan-jobs/internal/utils"
	"go.uber.org/zap"
	"os"
)

var Logger *zap.Logger

func LoggerSetup() *zap.Logger {
	if Logger != nil {
		return Logger
	}

	if configs.AppConfig == nil {
		fmt.Println("Fail to AppConfig not load!")
		os.Exit(1)
	}

	logger := utils.InitLog(
		configs.AppConfig.LoggerConfig.LogPath,
		configs.AppConfig.LoggerConfig.LogName,
		configs.AppConfig.Debug,
	)
	Logger = logger

	return Logger
}
