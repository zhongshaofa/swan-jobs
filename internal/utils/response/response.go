package response

import (
	"github.com/gin-gonic/gin"
	"github.com/zhongshaofa/swan-jobs/internal/request"
	"github.com/zhongshaofa/swan-jobs/internal/utils"
)

type response struct {
	ErrorCode int            `json:"error_code"`
	ErrorMsg  string         `json:"error_msg"`
	Data      interface{}    `json:"data"`
	Time      utils.Datetime `json:"time"`
}

type resultList struct {
	Count      int                       `json:"count"`
	Pagination request.PaginationRequest `json:"pagination"`
	List       interface{}               `json:"list"`
}

const codeSuccess = 0
const codeError = 1
const CodeNoLoginError = 10000
const CodeNoAllowError = 10001
const CodeValidateError = 10002

const successStatus = 200

func RespError(message string, c *gin.Context) {
	Resp(codeError, message, map[string]interface{}{}, c)
}

func RespValidateError(message string, c *gin.Context) {
	Resp(CodeValidateError, message, map[string]interface{}{}, c)
}
func RespOtherError(errorCode int, message string, c *gin.Context) {
	Resp(errorCode, message, map[string]interface{}{}, c)
}

func RespSuccess(data interface{}, c *gin.Context) {
	Resp(codeSuccess, "success", data, c)
}

func RespListSuccess(list interface{}, count int, p request.PaginationRequest, c *gin.Context) {
	Resp(codeSuccess, "success", resultList{
		Count:      count,
		Pagination: p,
		List:       list,
	}, c)
}

func Resp(errorCode int, message string, data interface{}, c *gin.Context) {
	if data == nil {
		data = make(map[string]interface{})
	}
	r := response{}
	r.ErrorCode = errorCode
	r.ErrorMsg = message
	r.Data = data
	r.Time = utils.NowDatetime()
	c.JSON(successStatus, r)
}
