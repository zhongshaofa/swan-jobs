package configs

import (
	"fmt"
	"gopkg.in/ini.v1"
	"os"
)

var SchedulerConfig *scheduler

type scheduler struct {
	RpcPort int
}

func LoadSchedulerConfig() {
	if AppConfig == nil {
		fmt.Println("Fail to AppConfig not load!")
		os.Exit(1)
	}

	cfg, err := ini.Load(AppConfig.ConfPath + "/scheduler.ini")
	if err != nil {
		fmt.Printf("Fail to read file scheduler.ini: %v", err)
		os.Exit(1)
	}

	port, _ := cfg.Section("scheduler-rpc").Key("port").Int()

	SchedulerConfig = &scheduler{
		RpcPort: port,
	}
}
