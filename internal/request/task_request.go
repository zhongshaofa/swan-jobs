package request

type TaskAddRequest struct {
	AppId        int    `form:"app_id" binding:"required" json:"app_id"`
	Manager      string `form:"manager" binding:"required" json:"manager"`       // 管理员
	Name         string `form:"name" binding:"required" json:"name"`             // 任务名称
	Mode         int    `form:"mode" binding:"required,min=1,max=2" json:"mode"` // 任务类型 1=守护进程 2=定时执行
	CronTimeOut  int    `form:"cron_time_out"  json:"cron_time_out"`                               // 仅针对 任务类型=定时执行
	CronFormula  string `form:"cron_formula"  json:"cron_formula"`                                 // 定时任务表达式
	ScheduleType int    `form:"schedule_type" binding:"required,min=1,max=3" json:"schedule_type"` // 调度类型 1=单机运行 2=空闲执行 3=分布式
	Directory    string `form:"directory" binding:"required" json:"directory"`                     // 执行目录
	Command      string `form:"command" binding:"required" json:"command"`                         // 执行命令
	Remark       string `form:"remark" json:"remark"`
}

type TaskEditRequest struct {
	TaskAddRequest
	IdFormRequest
}
