drop table if exists sj_user;
create table sj_user
(
    id         int(11)                                NOT NULL AUTO_INCREMENT,
    username   varchar(50)  default ''                not null comment '用户名',
    password   varchar(50)  default ''                not null comment '密码',
    status     tinyint(1)   default 0                 not null comment '状态 1=启用 2=停用',
    role_type  tinyint(1)   default 2                 not null comment '角色类型 1=管理员 2=普通角色',
    remark     varchar(255) default ''                not null comment '备注信息',
    created_at datetime     default CURRENT_TIMESTAMP not null,
    updated_at datetime     default CURRENT_TIMESTAMP not null,
    deleted_at datetime                               null,
    PRIMARY KEY (id),
    key idx_name (username)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment '人员管理';


drop table if exists sj_client;
create table sj_client
(
    id                 int(11)                                    NOT NULL AUTO_INCREMENT,
    ip                 varchar(50)  default ''                    not null comment 'IP地址',
    port               int          default 0                     not null comment '端口',
    client_name        varchar(50)  default ''                    not null comment 'client名',
    client_code        varchar(50)  default ''                    not null comment 'client编码',
    cpu_percent        int(5)                                     not null default 0 comment '空闲CPU',
    memory_percent     int(5)                                     not null default 0 comment '空闲内存',
    check_heartbeat_at datetime     default '1970-01-01 01:01:01' not null comment '健康检测时间',
    is_heartbeat       tinyint(1)   default 0                     not null comment '是否健康 1=是 2=否',
    status             tinyint(1)   default 0                     not null comment '状态 1=启用 2=停用',
    remark             varchar(255) default ''                    not null comment '备注信息',
    created_at         datetime     default CURRENT_TIMESTAMP     not null,
    updated_at         datetime     default CURRENT_TIMESTAMP     not null,
    deleted_at         datetime                                   null,
    PRIMARY KEY (id),
    UNIQUE KEY `idx_only` (`ip`, `port`, `deleted_at`),
    key idx_name (client_name),
    key idx_code (client_code)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment '客户端管理';


drop table if exists sj_application;
create table sj_application
(
    id           int(11)                                NOT NULL AUTO_INCREMENT,
    app_name     varchar(50)  default ''                not null comment '应用名',
    app_code     varchar(50)  default ''                not null comment '应用编码',
    status       tinyint(1)   default 0                 not null comment '状态 1=启用 2=停用',
    introduction varchar(255) default ''                not null comment '系统简介',
    remark       varchar(255) default ''                not null comment '备注信息',
    created_at   datetime     default CURRENT_TIMESTAMP not null,
    updated_at   datetime     default CURRENT_TIMESTAMP not null,
    deleted_at   datetime                               null,
    PRIMARY KEY (id),
    key idx_name (app_name),
    key idx_code (app_code)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment '应用管理';


drop table if exists sj_application_client;
create table sj_application_client
(
    id         int(11)  NOT NULL AUTO_INCREMENT,
    app_id     int(11)  NOT NULL DEFAULT '0' comment '应用ID',
    client_id  int(11)  NOT NULL DEFAULT '0' comment '客户端ID',
    created_at datetime          default CURRENT_TIMESTAMP not null,
    updated_at datetime          default CURRENT_TIMESTAMP not null,
    deleted_at datetime null,
    PRIMARY KEY (id),
    key idx_app_id (app_id),
    key idx_client_id (client_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment '应用-客户端';


drop table if exists sj_user_application;
CREATE TABLE sj_user_application
(
    id         int(11)  NOT NULL AUTO_INCREMENT,
    user_id    int(11)  NOT NULL DEFAULT '0' comment '用户ID',
    app_id     int(11)  NOT NULL DEFAULT '0' comment '应用ID',
    created_at datetime          default CURRENT_TIMESTAMP not null,
    updated_at datetime          default CURRENT_TIMESTAMP not null,
    deleted_at datetime null,
    PRIMARY KEY (id),
    key idx_app_id (app_id),
    key idx_user_id (user_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='用户应用权限';


drop table if exists sj_task;
create table sj_task
(
    id            int(11)      NOT NULL AUTO_INCREMENT,
    app_id        int(11)      NOT NULL default '0' comment '用户ID',
    name          varchar(255) not null default '' comment '任务名称',
    manager       varchar(50)  not null default '' comment '负责人',
    mode          tinyint(1)   NOT NULL default 1 COMMENT '任务类型 1=守护进程 2=定时执行',
    cron_time_out int(5)       NOT NULL default 6 comment '仅针对 任务类型=定时执行',
    cron_formula  varchar(50)  not null default '' comment '定时任务表达式',
    schedule_type tinyint(1)   not null default 1 comment '调度类型 1=单机运行 2=空闲执行 3=分布式',
    execute_type  tinyint(1)   not null default 1 comment '执行类型 1=直接执行 2=shell命令(无法守护进程)',
    directory     varchar(255) not null default '' comment '执行目录',
    command       varchar(255) not null default '' comment '执行命令',
    shell_source  mediumtext   null     default null comment 'shell命令源码',
    notify_id     tinyint(1)   not null default 0 comment '通知',
    status        tinyint(1)   NOT NULL DEFAULT 0 comment '调度状态：2-停止，1-运行',
    remark        varchar(255)          default '' not null comment '备注信息',
    create_uid    int(11)      NOT NULL DEFAULT '0',
    created_at    datetime              default CURRENT_TIMESTAMP not null,
    updated_at    datetime              default CURRENT_TIMESTAMP not null,
    deleted_at    datetime     null,
    PRIMARY KEY (id),
    key idx_app_id (app_id),
    key idx_manager (manager)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment '任务信息';


drop table if exists sj_task_schedule;
create table sj_task_schedule
(
    id                  bigint(20)   NOT NULL AUTO_INCREMENT,
    task_id             int(11)      NOT NULL DEFAULT '0' comment '任务ID',
    schedule_time       datetime     null comment '调度-时间',
    schedule_agent_list varchar(255) not null default '' comment '执行的客户端列表',
    schedule_status     tinyint(1)   NOT NULL DEFAULT 1 comment '调度-状态：2-失效，1-待执行 2-执行中 3-执行成功 99-执行失败',
    is_timeout          tinyint(1)   NOT NULL DEFAULT 0 comment '是否超时：2-否，1-是',
    remark              varchar(255)          default '' not null comment '备注信息',
    created_at          datetime              default CURRENT_TIMESTAMP not null,
    updated_at          datetime              default CURRENT_TIMESTAMP not null,
    deleted_at          datetime     null,
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment '任务调度';


drop table if exists sj_task_schedule_log;
create table sj_task_schedule_log
(
    id          bigint(20)   NOT NULL AUTO_INCREMENT,
    task_id     int(11)      NOT NULL DEFAULT '0' comment '任务ID',
    schedule_id int(11)      NOT NULL DEFAULT '0' comment '调度ID',
    agent_id    int(11)      NOT NULL DEFAULT '0' comment '客户端ID',
    lot_id      int(11)      NOT NULL DEFAULT '0' comment '日志批次ID',
    log_path    varchar(255) NOT NULL DEFAULT '' comment '日志目录',
    exec_date   date         not null comment '执行日期',
    created_at  datetime              default CURRENT_TIMESTAMP not null,
    updated_at  datetime              default CURRENT_TIMESTAMP not null,
    deleted_at  datetime     null,
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment '任务调度日志';


drop table if exists sj_notify;
create table sj_notify
(
    id         int(11)      NOT NULL AUTO_INCREMENT,
    name       varchar(255) not null default '',
    type       tinyint(1)   not null default 1 comment '通知类型 1=邮件通知 2=短信通知 3=微信通知 4=钉钉通知 5=自定义回调',
    email      varchar(255) not null default '',
    phone      varchar(255) not null default '',
    remark     varchar(255) not null default '',
    status     tinyint(1)   NOT NULL DEFAULT 0 comment '状态：2-停止，1-运行',
    created_at datetime              default CURRENT_TIMESTAMP not null,
    updated_at datetime              default CURRENT_TIMESTAMP not null,
    deleted_at datetime     null,
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment '通知';

# 管理员
INSERT INTO sj_user(id, username, password, status, role_type)
VALUES (1, 'admin', 'e46d6f876514ec1fb9e246331d5a2c2b', 1, 1),
       (2, 'test', 'e46d6f876514ec1fb9e246331d5a2c2b', 1, 2);

# 客户端
INSERT INTO sj_client (id, ip, port, client_name, client_code, status)
VALUES (1, '127.0.0.1', 25680, '本地执行器', 'localhost', 1);

INSERT INTO sj_application (`id`, `app_name`, `app_code`, `status`, `introduction`)
VALUES (1, '测试项目', 'test_project', 1, '这是一个应用简介');

INSERT INTO sj_application_client (`id`, `app_id`, client_id)
VALUES (1, 1, 1);

INSERT INTO sj_task (`id`, `app_id`, `name`, `mode`, `cron_time_out`, `cron_formula`, `schedule_type`, `execute_type`,
                     `directory`, `command`, `status`)
VALUES (1, 1, '测试-cron', 2, 6, '*/5 * * * * ?', 1, 1, '/tmp/scripts',
        'php cron_test.php', 1),
       (2, 1, '测试-supervisor', 1, 6, '', 1, 1, '/tmp/scripts',
        'php supervisor_test.php', 1);