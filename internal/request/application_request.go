package request

type ApplicationAddRequest struct {
	AppName      string `form:"app_name" json:"app_name" binding:"required"`
	AppCode      string `form:"app_code" json:"app_code" binding:"required"`
	Introduction string `form:"introduction" json:"introduction" binding:"required"`
	Remark       string `form:"remark" json:"remark"`
	Status       int    `form:"status" json:"status" binding:"required,min=1,max=2"`
}

type ApplicationEditRequest struct {
	ApplicationAddRequest
	IdFormRequest
}

type ApplicationClientAuthRequest struct {
	IdFormRequest
	ClientIds []int `form:"client_ids" json:"client_ids" binding:"required"`
}
