package models

import (
	"github.com/zhongshaofa/swan-jobs/configs"
)

type Application struct {
	Base
	AppName      string              `gorm:"column:app_name;NOT NULL" json:"app_name"`
	AppCode      string              `gorm:"column:app_code;NOT NULL" json:"app_code"`
	Introduction string              `gorm:"column:introduction;NOT NULL" json:"introduction"` // 简介
	Remark       string              `gorm:"column:remark;NOT NULL" json:"remark"`             // 备注信息
	Status       int                 `gorm:"column:status;default:1;NOT NULL" json:"status"`
	ClientList   []ApplicationClient `gorm:"foreignKey:AppId;references:ID" json:"client_list"` //关联应用客户端列表
}

func (*Application) TableName() string {
	return configs.DatabaseConfig.Prefix + "application"
}
