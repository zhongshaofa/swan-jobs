package test

import (
	"fmt"
	"os"
	"syscall"
	"testing"
)

func TestProcess(t *testing.T) {
	process, err := os.FindProcess(7032)
	if err != nil {
		fmt.Printf("process err:%+v\n", err)
		return
	}
	signalErr := process.Signal(syscall.SIGCHLD)
	fmt.Printf("process syscall:%+v\n", signalErr)
}
