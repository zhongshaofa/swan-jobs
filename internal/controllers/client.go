package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/zhongshaofa/swan-jobs/internal/models"
	"github.com/zhongshaofa/swan-jobs/internal/repositorys"
	"github.com/zhongshaofa/swan-jobs/internal/request"
	"github.com/zhongshaofa/swan-jobs/internal/utils/response"
	"github.com/zhongshaofa/swan-jobs/internal/utils/validator"
	"strconv"
)

type client struct {
	clientRepository repositorys.ClientRepository
}

var Client = client{}

func (c *client) List(gc *gin.Context) {
	p := request.GetPaginationRequest(gc)
	m := models.Client{}

	clientName, _ := gc.GetQuery("client_name")
	clientCode, _ := gc.GetQuery("client_code")
	status, _ := gc.GetQuery("status")
	isHeartbeat, _ := gc.GetQuery("is_heartbeat")
	if len(clientName) > 0 {
		m.ClientName = clientName
	}
	if len(clientCode) > 0 {
		m.ClientCode = clientCode
	}
	if len(status) > 0 {
		statusInt, _ := strconv.Atoi(status)
		m.Status = statusInt
	}
	if len(isHeartbeat) > 0 {
		isHeartbeatInt, _ := strconv.Atoi(isHeartbeat)
		m.IsHeartbeat = isHeartbeatInt
	}
	list, count, _ := c.clientRepository.GetList(&m, p)
	response.RespListSuccess(list, count, p, gc)
	return
}

func (c *client) Add(gc *gin.Context) {
	var r request.ClientAddRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	_, err = c.clientRepository.Create(&models.Client{
		Ip:          r.Ip,
		Port:        r.Port,
		ClientName:  r.ClientName,
		ClientCode:  r.ClientCode,
		IsHeartbeat: models.False,
		Status:      r.Status,
		Remark:      r.Remark,
	})
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *client) Edit(gc *gin.Context) {
	var r request.ClientEditRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	u, err := c.clientRepository.Detail(r.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}

	u.Ip = r.Ip
	u.Port = r.Port
	u.ClientName = r.ClientName
	u.ClientCode = r.ClientCode
	u.Status = r.Status
	u.Remark = r.Remark

	err = c.clientRepository.Update(u)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess("", gc)
	return
}

func (c *client) Detail(gc *gin.Context) {
	idQueryRequest := request.GetIdQueryRequest(gc)
	if idQueryRequest == nil {
		response.RespError("id不能为空", gc)
		return
	}

	detail, err1 := c.clientRepository.Detail(idQueryRequest.Id)
	if err1 != nil {
		response.RespError(err1.Error(), gc)
		return
	}
	response.RespSuccess(detail, gc)
	return
}

func (c *client) Delete(gc *gin.Context) {
	var r request.IdsFormRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	err = c.clientRepository.Delete(r.Ids)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess("", gc)
	return
}

func (c *client) SwitchStatus(gc *gin.Context) {
	var r request.SwitchStatusFormRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	err = c.clientRepository.SwitchStatus(r.Ids, r.Status)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}
