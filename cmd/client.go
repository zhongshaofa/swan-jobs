package main

import (
	"flag"
	"fmt"
	"github.com/zhongshaofa/swan-jobs/configs"
	"github.com/zhongshaofa/swan-jobs/internal"
	"github.com/zhongshaofa/swan-jobs/internal/plugin"
	"github.com/zhongshaofa/swan-jobs/internal/rpc/server/client"
	"github.com/zhongshaofa/swan-jobs/internal/services"
	"github.com/zhongshaofa/swan-jobs/internal/utils"
)

func init() {
	confPath := flag.String("conf", "../conf", "conf 配置目录")
	flag.Parse()

	go internal.AppSignal()

	loadClientConfig(confPath)
	pluginClientSetup()
}

func main() {
	services.GetClientSupervisorTaskManageInstance().AllStart()

	go services.ClientChanListening()
	go services.GetClientSupervisorTaskManageInstance().Refresh()

	rpcPort := fmt.Sprintf(":%d", configs.ClientConfig.RpcPort)
	utils.PrintSystemDescribe(utils.ServeNameClient, rpcPort)
	client.Start(rpcPort)
}

func loadClientConfig(confPath *string) {
	configs.LoadAppConfig(*confPath)
	configs.LoadClientConfig()
}

func pluginClientSetup() {
	plugin.LoggerSetup()
}
