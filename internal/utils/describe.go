package utils

import (
	"fmt"
	"os"
)

var (
	ServeNameBackend   = "SwanBackend"
	ServeNameClient    = "SwanClient"
	ServeNameScheduler = "SwanScheduler"
)

var version = "v1.0.0 Beat"

var githubUrl = "https://github.com/zhongshaofa/swan-jobs"
var giteeUrl = "https://gitee.com/zhongshaofa/swan-jobs"

var systemLogo = `
                               _       _         
 _____      ____ _ _ __       (_) ___ | |__  ___ 
/ __\ \ /\ / / _' | '_ \ _____| |/ _ \| '_ \/ __|
\__ \\ V  V / (_| | | | |_____| | (_) | |_) \__ \
|___/ \_/\_/ \__,_|_| |_|    _/ |\___/|_.__/|___/
                            |__/
`

func PrintSystemDescribe(serveName string, addr string) {
	fmt.Println("======================================================")
	fmt.Printf("%s\n", systemLogo)
	fmt.Println("Serve     Name:", serveName)
	fmt.Println("Serve  Address:", addr)
	fmt.Println("Serve      Pid:", os.Getpid())
	fmt.Println("System Version:", version)
	fmt.Println("Github Address:", githubUrl)
	fmt.Println("Gitee  Address:", giteeUrl)
	fmt.Println("======================================================")
}
