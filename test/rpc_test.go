package test

import (
	"fmt"
	"github.com/zhongshaofa/swan-jobs/internal/rpc/client"
	pc "github.com/zhongshaofa/swan-jobs/internal/rpc/proto/client"
	"testing"
)

// TestClientRpcClient 测试客户端的RPC服务
func TestClientRpcClient(t *testing.T) {

	clientRpcConnectAddress := "localhost:15678"

	testRequest := pc.TaskRequest{
		Command: "php think test1",
		Timeout: 60,
		TaskId:  111,
		Mode:    1,
	}

	heartbeatRequest := pc.HeartbeatRequest{ClientId: 66}

	_, err1 := client.RunTaskByScheduler(clientRpcConnectAddress, &testRequest)
	_, err2 := client.RunTaskByScheduler(clientRpcConnectAddress, &testRequest)

	res3, err3 := client.HeartbeatByScheduler(clientRpcConnectAddress, &heartbeatRequest)

	if err1 != nil {
		t.Error(err1)
	}

	if err2 != nil {
		t.Error(err2)
	}

	if err3 != nil {
		t.Error(err3)
	}

	fmt.Println("agent info:", res3)
}