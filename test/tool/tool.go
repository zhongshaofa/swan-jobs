package tool

import (
	"github.com/zhongshaofa/swan-jobs/configs"
	"github.com/zhongshaofa/swan-jobs/internal/plugin"
)

func InitDB()  {
	confPath := "../../deployments/conf"
	configs.LoadAppConfig(confPath)
	configs.LoadDatabaseConfig()
	plugin.ModelSetup()
}
