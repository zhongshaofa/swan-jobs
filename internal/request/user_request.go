package request

type UserAddRequest struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
	RoleType int    `form:"role_type" json:"role_type" binding:"required,min=1,max=2"`
	Status   int    `form:"status" json:"status" binding:"required"`
	Remark   string `form:"remark" json:"remark" binding:"required"`
}

type UserEditRequest struct {
	RoleType int    `form:"role_type" json:"role_type" binding:"required,min=1,max=2"`
	Status   int    `form:"status" json:"status" binding:"required"`
	Remark   string `form:"remark" json:"remark"`
	IdFormRequest
}

type UserRestartPasswordRequest struct {
	IdFormRequest
	Password      string `form:"password" json:"password" binding:"required"`
	AgainPassword string `form:"again_password" json:"again_password" binding:"required"`
}

type UserApplicationAuthRequest struct {
	IdFormRequest
	ApplicationIds []int `form:"application_ids" json:"application_ids" binding:"required"`
}
