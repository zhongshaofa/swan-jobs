package models

import (
	"github.com/zhongshaofa/swan-jobs/configs"
)

type UserApplication struct {
	Base
	UserId      int         `gorm:"column:user_id;default:0;NOT NULL" json:"user_id"`  //用户ID
	AppId       int         `gorm:"column:app_id;default:0;NOT NULL" json:"app_id"`    //应用ID
	Application Application `gorm:"foreignKey:AppId;references:ID" json:"application"` //关联应用
}

func (m *UserApplication) TableName() string {
	return configs.DatabaseConfig.Prefix + "user_application"
}
