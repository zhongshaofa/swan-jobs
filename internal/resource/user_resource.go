package resource

import "github.com/zhongshaofa/swan-jobs/internal/models"

type UserApplicationListResource struct {
	List []applicationList `json:"list"`
}

type applicationList struct {
	AppId   int    `json:"app_id"`
	AppName string `json:"app_name"`
	AppCode string `json:"app_code"`
}

func UserApplicationListResourceCreate(user models.User) UserApplicationListResource {
	list := make([]applicationList, 0)
	for _, userApplication := range user.ApplicationList {
		list = append(list, applicationList{
			AppId:   userApplication.AppId,
			AppName: userApplication.Application.AppName,
			AppCode: userApplication.Application.AppCode,
		})
	}
	return UserApplicationListResource{List: list}
}
