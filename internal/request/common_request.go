package request

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

type IdFormRequest struct {
	Id int `form:"id" json:"id" binding:"required"`
}

type IdsFormRequest struct {
	Ids []int `form:"ids" json:"ids" binding:"required"`
}

type SwitchStatusFormRequest struct {
	IdsFormRequest
	Status int `form:"status" json:"status" binding:"required"`
}

type IdQueryRequest struct {
	Id int `query:"id" binding:"required"`
}

func GetIdQueryRequest(gc *gin.Context) *IdQueryRequest {
	idString, exists := gc.GetQuery("id")
	id, err := strconv.Atoi(idString)
	if !exists || err != nil {
		return nil
	}
	return &IdQueryRequest{
		Id: id,
	}
}
