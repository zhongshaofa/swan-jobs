package repositorys

import (
	"github.com/zhongshaofa/swan-jobs/internal/models"
	"github.com/zhongshaofa/swan-jobs/internal/plugin"
)

type TaskScheduleInterface interface {
	Create(taskSchedule *models.TaskSchedule) (*models.TaskSchedule, error)
}

type TaskScheduleRepository struct {
}

func (TaskScheduleRepository) Create(taskSchedule *models.TaskSchedule) (*models.TaskSchedule, error) {
	result := plugin.DB.Create(&taskSchedule)
	if result.Error != nil {
		return nil, result.Error
	}
	return taskSchedule, nil
}
