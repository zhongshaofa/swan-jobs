package system

import (
	"github.com/shirou/gopsutil/v3/cpu"
	"github.com/shirou/gopsutil/v3/mem"
	"time"
)

func GetUsedCpuPercent() float64 {
	percent, _ := cpu.Percent(time.Second, false)
	return percent[0]
}

func GetIdleCpuPercent() float64 {
	return 100 - GetUsedCpuPercent()
}

func GetUsedMemoryPercent() float64 {
	memInfo, _ := mem.VirtualMemory()
	return memInfo.UsedPercent
}

func GetIdleMemoryPercent() float64 {
	return 100 - GetUsedMemoryPercent()
}
