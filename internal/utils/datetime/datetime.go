package datetime

import "time"

func CurrentTime() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

func FormatDatetime(time time.Time) string {
	return time.Format("2006-01-02 15:04:05")
}
