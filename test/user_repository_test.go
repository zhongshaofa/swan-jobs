package test

import (
	"fmt"
	"github.com/zhongshaofa/swan-jobs/internal/repositorys"
	"github.com/zhongshaofa/swan-jobs/test/tool"
	"testing"
)

func TestUserLogin(t *testing.T) {
	tool.InitDB()
	repository := repositorys.UserRepository{}

	user, _ := repository.UserLogin("admin", "21232f297a57a5a743894a0e4a801fc3")
	fmt.Println(user)
}
