package validator

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/zhongshaofa/swan-jobs/internal/plugin"
)

// FormatValidatorError 格式化校验错误消息
func FormatValidatorError(err error) string {
	errMsg := ""
	for _, validatorError := range err.(validator.ValidationErrors) {
		msg, _ := plugin.ValidatorTranslator.T(
			validatorError.Tag(),
			validatorError.Field(),
			validatorError.Param(),
		)
		if len(errMsg) == 0 {
			errMsg = msg
		} else {
			errMsg = fmt.Sprintf("%s,%s", errMsg, msg)
		}
	}
	return errMsg
}
