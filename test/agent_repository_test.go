package test

import (
	"fmt"
	"github.com/zhongshaofa/swan-jobs/internal/repositorys"
	"github.com/zhongshaofa/swan-jobs/test/tool"
	"testing"
)

func TestGetAgentById(t *testing.T) {
	tool.InitDB()

	repository := repositorys.ClientInterface(repositorys.ClientRepository{})

	agent, err := repository.Detail(1)

	if err != nil {
		t.Error(err)
	}

	fmt.Println("agent:", agent)
}
