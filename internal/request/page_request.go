package request

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

type PaginationRequest struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
}

// GetPaginationRequest 获取分页参数
func GetPaginationRequest(gc *gin.Context) PaginationRequest {
	pageString, _ := gc.GetQuery("page")
	limitString, _ := gc.GetQuery("limit")

	page, limit := 1, 20
	if len(pageString) > 0 {
		pageInt, err := strconv.Atoi(pageString)
		if err == nil {
			page = pageInt
		}
	}
	if len(limitString) > 0 {
		limitInt, err := strconv.Atoi(limitString)
		if err == nil {
			limit = limitInt
		}
	}

	return PaginationRequest{
		Page:  page,
		Limit: limit,
	}
}
