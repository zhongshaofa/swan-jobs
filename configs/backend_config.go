package configs

import (
	"fmt"
	"gopkg.in/ini.v1"
	"os"
	"time"
)

var BackendConfig *backend

type backend struct {
	SchedulerPort int
	ServeConfig   *serveConfig
}

type serveConfig struct {
	Mode         string
	Port         int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

// LoadBackendConfig 加载后台配置
func LoadBackendConfig() {
	if AppConfig == nil {
		fmt.Println("Fail to AppConfig not load!")
		os.Exit(1)
	}

	cfg, err := ini.Load(AppConfig.ConfPath + "/backend.ini")
	if err != nil {
		fmt.Printf("Fail to read file backend.ini: %v", err)
		os.Exit(1)
	}

	schedulerPort, _ := cfg.Section("scheduler").Key("scheduler_port").Int()

	mode := cfg.Section("serve").Key("mode").String()
	port, _ := cfg.Section("serve").Key("port").Int()
	readTimeout, _ := cfg.Section("serve").Key("port").Int64()
	writeTimeout, _ := cfg.Section("serve").Key("port").Int64()

	BackendConfig = &backend{
		SchedulerPort: schedulerPort,
		ServeConfig: &serveConfig{
			Mode:         mode,
			Port:         port,
			ReadTimeout:  time.Duration(readTimeout),
			WriteTimeout: time.Duration(writeTimeout),
		},
	}
}
