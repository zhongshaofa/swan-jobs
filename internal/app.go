package internal

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

// AppSignal 监听信号
func AppSignal() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGCHLD)
	for {
		s := <-c
		fmt.Println("收到信号 -- ", s)
		switch s {
		case syscall.SIGCHLD:
			//go command.SupervisorList.UpdateStatusBySignal()
		case syscall.SIGINT, syscall.SIGTERM:
			shutdown()
		}
	}
}

// 应用退出
func shutdown() {

	defer func() {
		fmt.Println("已退出")
		os.Exit(0)
	}()

	fmt.Println("应用准备退出")
	// 停止所有任务调度
	fmt.Println("停止定时任务调度")
}
