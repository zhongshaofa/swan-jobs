package test

import (
	"github.com/zhongshaofa/swan-jobs/internal/services"
	"testing"
)

func TestCommandExec(t *testing.T) {
	go services.ClientChanListening()

	parameter := services.ClientParameter{
		Directory:  "/Users/10021911/GolandProjects/swan-jobs/scripts",
		Command:    "php supervisor_test.php",
		Timeout:    5,
		TaskId:     1,
		ScheduleId: 1,
		Mode:       1,
	}

	services.Exec(parameter)
}
