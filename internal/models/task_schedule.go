package models

import (
	"github.com/zhongshaofa/swan-jobs/configs"
	"time"
)

type TaskSchedule struct {
	Base
	TaskId            int       `gorm:"column:task_id;default:0;NOT NULL" json:"task_id"`
	ScheduleTime      time.Time `gorm:"column:schedule_time" json:"schedule_time"`                        // 调度-时间
	ScheduleAgentList string    `gorm:"column:schedule_agent_list;NOT NULL" json:"schedule_agent_list"`   // 调度-结果
	ScheduleStatus    int       `gorm:"column:schedule_status;default:1;NOT NULL" json:"schedule_status"` // 调度-状态：4-失效，1-代执行 2-执行中 3-执行成功 99-执行失败
	IsTimeout         int       `gorm:"column:is_timeout;default:0;NOT NULL" json:"is_timeout"`           // 是否超时：2-否，1-是
	Remark            string    `gorm:"column:remark;NOT NULL" json:"remark"`                             // 备注信息
}

func (m *TaskSchedule) TableName() string {
	return configs.DatabaseConfig.Prefix + "task_schedule"
}
