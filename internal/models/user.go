package models

import (
	"github.com/zhongshaofa/swan-jobs/configs"
)

const (
	RoleTypeAdmin     = 1
	RoleTypeUniversal = 2
)

type User struct {
	Base
	Username        string            `gorm:"column:username" json:"username" json:"username"`                       //用户名
	Password        string            `gorm:"column:password" json:"-" json:"password"`                              //密码
	Status          int               `gorm:"column:status;default:1" json:"status" json:"status"`                   //状态
	RoleType        int               `gorm:"column:role_type;default:2;NOT NULL" json:"role_type" json:"role_type"` // 角色类型 1=管理员 2=普通角色
	Remark          string            `gorm:"column:remark;NOT NULL" json:"remark"`                                  // 备注信息
	ApplicationList []UserApplication `gorm:"foreignKey:UserId;references:ID" json:"application_list"`               //关联用户授权应用列表
}

func (*User) TableName() string {
	return configs.DatabaseConfig.Prefix + "user"
}
