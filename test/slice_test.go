package test

import (
	"fmt"
	"github.com/zhongshaofa/swan-jobs/internal/models"
	"github.com/zhongshaofa/swan-jobs/internal/services"
	"github.com/zhongshaofa/swan-jobs/test/tool"
	"sync"
	"testing"
	"time"
)

var sliceLock sync.Mutex

func TestSlice(t *testing.T) {

	var a []int
	for i := 0; i < 10000; i++ {
		go func() {
			sliceLock.Lock()
			a = append(a, 1) // 多协程并发读写slice
			sliceLock.Unlock()
		}()
	}


	time.Sleep(time.Duration(1)*time.Second)

	fmt.Println(len(a))

}

func TestCronTaskManage(t *testing.T) {
	cronTaskManage := services.NewCronTaskManage()

	for i := 0; i < 10000; i++ {
		cId := i
		go func() {
			cronTaskManage.Add(&models.Task{
				Base: models.Base{
					ID: uint(cId),
				},
				AppId:        cId,
				Name:         "test",
				Mode:         2,
				ScheduleType: 0,
			})
		}()
	}

	time.Sleep(time.Duration(1) * time.Second)

	fmt.Println(cronTaskManage.Len())
}


func TestSupervisorTaskManage(t *testing.T) {
	tool.InitDB()
	supervisorTaskManage := services.NewSupervisorTaskManage()

	for i := 0; i < 10000; i++ {
		cId := i
		go func() {
			supervisorTaskManage.Add(&models.Task{
				Base: models.Base{
					ID: uint(cId),
				},
				AppId:        cId,
				Name:         "test",
				Mode:         2,
				ScheduleType: 0,
			})
		}()
	}

	time.Sleep(time.Duration(1) * time.Second)

	fmt.Println(supervisorTaskManage.Len())
}