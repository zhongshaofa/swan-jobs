package request

type LoginRequest struct {
	Username string `form:"username" comment:"用户名" json:"username" binding:"required"`
	Password string `form:"password" comment:"密码" json:"password" binding:"required"`
}
