package models

import (
	"github.com/zhongshaofa/swan-jobs/configs"
)

type ApplicationClient struct {
	Base
	AppId    int    `gorm:"column:app_id;default:0;NOT NULL" json:"app_id"`       //应用ID
	ClientId int    `gorm:"column:client_id;default:0;NOT NULL" json:"client_id"` //客户端ID
	Client   Client `gorm:"foreignKey:ClientId;references:ID" json:"client"`      //关联客户端
}

func (*ApplicationClient) TableName() string {
	return configs.DatabaseConfig.Prefix + "application_client"
}
