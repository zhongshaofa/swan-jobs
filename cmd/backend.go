package main

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/zhongshaofa/swan-jobs/configs"
	"github.com/zhongshaofa/swan-jobs/internal"
	"github.com/zhongshaofa/swan-jobs/internal/plugin"
	"github.com/zhongshaofa/swan-jobs/internal/routers"
	"github.com/zhongshaofa/swan-jobs/internal/utils"
	"net/http"
	"time"
)

func init() {
	confPath := flag.String("conf", "../conf", "conf 配置目录")
	flag.Parse()

	go internal.AppSignal()

	loadBackendConfig(confPath)
	pluginBackendSetup()
}

func main() {
	gin.SetMode(configs.BackendConfig.ServeConfig.Mode)
	router := routers.InitRouter()
	addr := fmt.Sprintf(":%d", configs.BackendConfig.ServeConfig.Port)
	server := &http.Server{
		Addr:           addr,
		Handler:        router,
		ReadTimeout:    configs.BackendConfig.ServeConfig.ReadTimeout * time.Second,
		WriteTimeout:   configs.BackendConfig.ServeConfig.WriteTimeout * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	utils.PrintSystemDescribe(utils.ServeNameBackend, addr)
	_ = server.ListenAndServe()
}

func loadBackendConfig(confPath *string) {
	configs.LoadAppConfig(*confPath)
	configs.LoadBackendConfig()
	configs.LoadDatabaseConfig()
}

func pluginBackendSetup() {
	plugin.LoggerSetup()
	plugin.ModelSetup()
	plugin.ValidatorSetup()
}
