package configs

import (
	"fmt"
	"gopkg.in/ini.v1"
	"os"
)

var ClientConfig *client

type client struct {
	RpcPort        int
	ConsoleLogPath string
}

func LoadClientConfig() {
	if AppConfig == nil {
		fmt.Println("Fail to AppConfig not load!")
		os.Exit(1)
	}

	cfg, err := ini.Load(AppConfig.ConfPath + "/client.ini")
	if err != nil {
		fmt.Printf("Fail to read file client.ini: %v", err)
		os.Exit(1)
	}

	port, _ := cfg.Section("client-rpc").Key("port").Int()
	consoleLogPath := cfg.Section("exec").Key("console_log_path").String()

	ClientConfig = &client{
		RpcPort:        port,
		ConsoleLogPath: consoleLogPath,
	}
}
