package models

import (
	"github.com/zhongshaofa/swan-jobs/configs"
	"time"
)

const StatusEnable = 1

const StatusDisable = 2

type Client struct {
	Base
	Ip               string    `gorm:"column:ip;NOT NULL" json:"ip"`
	Port             int       `gorm:"column:port;default:0;NOT NULL" json:"port"`
	ClientName       string    `gorm:"column:client_name" json:"client_name"`
	ClientCode       string    `gorm:"column:client_code" json:"client_code"`
	CpuPercent       int       `gorm:"column:cpu_percent;default:0;NOT NULL" json:"cpu_percent"`
	MemoryPercent    int       `gorm:"column:memory_percent;default:0;NOT NULL" json:"memory_percent"`
	CheckHeartbeatAt time.Time `gorm:"column:check_heartbeat_at;default:1970-01-01 01:01:01;NOT NULL" json:"check_heartbeat_at"`
	IsHeartbeat      int       `gorm:"column:is_heartbeat;default:0" json:"is_heartbeat"` // 是否健康 1=是 2=否
	Status           int       `gorm:"column:status;default:1" json:"status"`             // 状态 1=启用 2=停用
	Remark string `gorm:"column:remark;NOT NULL" json:"remark"` // 备注信息
}

func (*Client) TableName() string {
	return configs.DatabaseConfig.Prefix + "client"
}

// CalculateIdleScore 获取客户端空闲分数值
func (client *Client) CalculateIdleScore() int {
	cpuScore := client.CpuPercent * getWeightsPercent(client.CpuPercent) / 100
	memoryScore := client.MemoryPercent * getWeightsPercent(client.MemoryPercent) / 100
	return cpuScore + memoryScore
}

// 获取空闲值权重比
func getWeightsPercent(idleValue int) int {
	if idleValue <= 10 {
		return 20
	} else if idleValue <= 20 {
		return 40
	} else if idleValue <= 30 {
		return 60
	} else if idleValue <= 40 {
		return 80
	} else {
		return 100
	}
}
