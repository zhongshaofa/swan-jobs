package resource

import "github.com/zhongshaofa/swan-jobs/internal/models"

type ApplicationClientListResource struct {
	List []clientList `json:"list"`
}

type clientList struct {
	ClientId   int    `json:"client_id"`
	ClientName string `json:"client_name"`
	ClientCode string `json:"client_code"`
}

func ApplicationClientListResourceCreate(application models.Application) ApplicationClientListResource {
	list := make([]clientList, 0)
	for _, applicationClient := range application.ClientList {
		list = append(list, clientList{
			ClientId:   applicationClient.ClientId,
			ClientName: applicationClient.Client.ClientName,
			ClientCode: applicationClient.Client.ClientName,
		})
	}
	return ApplicationClientListResource{List: list}
}
