package handle

import (
	"github.com/gin-gonic/gin"
	"github.com/zhongshaofa/swan-jobs/internal/utils/response"
)

func NoRouteHandle(gc *gin.Context) {
	response.RespError("不存在该路由", gc)
	return
}

func NoMethodHandle(gc *gin.Context) {
	response.RespError("不存在该方法", gc)
	return
}
