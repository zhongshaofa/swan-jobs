package client

import (
	"context"
	pc "github.com/zhongshaofa/swan-jobs/internal/rpc/proto/client"
	"google.golang.org/grpc"
	"log"
	"time"
)

// RunTaskByScheduler 后续优化成连接池
func RunTaskByScheduler(connectAddress string, request *pc.TaskRequest) (*pc.TaskResponse, error) {
	timeout := int(request.Timeout)
	if timeout <= 0 || timeout > 86400 {
		timeout = 86400
	}

	ctx, cancel := context.WithTimeout(context.TODO(), time.Duration(timeout)*time.Second)
	defer cancel()

	conn, err := grpc.DialContext(ctx, connectAddress, grpc.WithBlock(), grpc.WithInsecure())
	if err != nil {
		log.Println("did not connect:", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		_ = conn.Close()
	}(conn)

	rpcServer := pc.NewServiceClient(conn)
	result, err := rpcServer.RunTask(ctx, request)

	return result, err
}

func HeartbeatByScheduler(connectAddress string, request *pc.HeartbeatRequest) (*pc.HeartbeatResponse, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*2)
	defer cancel()

	conn, err := grpc.DialContext(ctx, connectAddress, grpc.WithBlock(), grpc.WithInsecure())
	if err != nil {
		log.Println("did not connect:", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		_ = conn.Close()
	}(conn)

	rpcServer := pc.NewServiceClient(conn)
	result, err := rpcServer.Heartbeat(ctx, request)

	return result, err
}

func ExistTaskByScheduler(connectAddress string, request *pc.CommonTaskRequest) (*pc.ExistTaskResponse, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*2)
	defer cancel()

	conn, err := grpc.DialContext(ctx, connectAddress, grpc.WithBlock(), grpc.WithInsecure())
	if err != nil {
		log.Println("did not connect:", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		_ = conn.Close()
	}(conn)

	rpcServer := pc.NewServiceClient(conn)
	result, err := rpcServer.ExistTask(ctx, request)

	return result, err
}

func ReloadTaskByScheduler(connectAddress string, request *pc.TaskRequest) (*pc.TaskResponse, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*2)
	defer cancel()

	conn, err := grpc.DialContext(ctx, connectAddress, grpc.WithBlock(), grpc.WithInsecure())
	if err != nil {
		log.Println("did not connect:", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		_ = conn.Close()
	}(conn)

	rpcServer := pc.NewServiceClient(conn)
	result, err := rpcServer.ReloadTask(ctx, request)

	return result, err
}

func StopTaskByScheduler(connectAddress string, request *pc.CommonTaskRequest) (*pc.TaskResponse, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*2)
	defer cancel()

	conn, err := grpc.DialContext(ctx, connectAddress, grpc.WithBlock(), grpc.WithInsecure())
	if err != nil {
		log.Println("did not connect:", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		_ = conn.Close()
	}(conn)

	rpcServer := pc.NewServiceClient(conn)
	result, err := rpcServer.StopTask(ctx, request)

	return result, err
}

func StopAllTaskByScheduler(connectAddress string, request *pc.CommonTaskRequest) (*pc.TaskResponse, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*2)
	defer cancel()

	conn, err := grpc.DialContext(ctx, connectAddress, grpc.WithBlock(), grpc.WithInsecure())
	if err != nil {
		log.Println("did not connect:", err)
		return nil, err
	}

	defer func(conn *grpc.ClientConn) {
		_ = conn.Close()
	}(conn)

	rpcServer := pc.NewServiceClient(conn)
	result, err := rpcServer.StopAllTask(ctx, request)

	return result, err
}
