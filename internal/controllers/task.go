package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/zhongshaofa/swan-jobs/internal/models"
	"github.com/zhongshaofa/swan-jobs/internal/repositorys"
	"github.com/zhongshaofa/swan-jobs/internal/request"
	"github.com/zhongshaofa/swan-jobs/internal/utils/response"
	"github.com/zhongshaofa/swan-jobs/internal/utils/validator"
	"strconv"
)

type task struct {
	taskRepository repositorys.TaskRepository
}

var Task = task{}

func (c *task) List(gc *gin.Context) {
	p := request.GetPaginationRequest(gc)
	m := models.Task{}

	name, _ := gc.GetQuery("name")
	manager, _ := gc.GetQuery("manager")
	status, _ := gc.GetQuery("status")
	appId, _ := gc.GetQuery("app_id")
	if len(name) > 0 {
		m.Name = name
	}
	if len(manager) > 0 {
		m.Manager = manager
	}
	if len(status) > 0 {
		statusInt, _ := strconv.Atoi(status)
		m.Status = statusInt
	}
	if len(appId) > 0 {
		appIdInt, _ := strconv.Atoi(appId)
		m.AppId = appIdInt
	} else {
		response.RespValidateError("应用ID必须填写", gc)
		return
	}

	list, count, _ := c.taskRepository.GetList(&m, p)
	response.RespListSuccess(list, count, p, gc)
	return
}

func (c *task) Add(gc *gin.Context) {
	var r request.TaskAddRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	_, err = c.taskRepository.Create(&models.Task{
		AppId:        r.AppId,
		Name:         r.Name,
		Manager:      r.Manager,
		Mode:         r.Mode,
		CronTimeOut:  r.CronTimeOut,
		CronFormula:  r.CronFormula,
		ScheduleType: r.ScheduleType,
		Directory:    r.Directory,
		Command:      r.Command,
		Remark:       r.Remark,
		Status:       models.False,
	})
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *task) Edit(gc *gin.Context) {
	var r request.TaskEditRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	u, err := c.taskRepository.Detail(r.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}

	u.AppId = r.AppId
	u.Name = r.Name
	u.Manager = r.Manager
	u.Mode = r.Mode
	u.CronTimeOut = r.CronTimeOut
	u.CronFormula = r.CronFormula
	u.ScheduleType = r.ScheduleType
	u.Directory = r.Directory
	u.Command = r.Command
	u.Remark = r.Remark

	err = c.taskRepository.Update(u)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *task) Detail(gc *gin.Context) {
	idQueryRequest := request.GetIdQueryRequest(gc)
	if idQueryRequest == nil {
		response.RespError("id不能为空", gc)
		return
	}

	detail, err := c.taskRepository.Detail(idQueryRequest.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(detail, gc)
	return
}

func (c *task) Delete(gc *gin.Context) {
	var r request.IdsFormRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}

	err = c.taskRepository.Delete(r.Ids)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess("", gc)
	return
}

func (c *task) SwitchStatus(gc *gin.Context) {
	var r request.SwitchStatusFormRequest
	err := gc.ShouldBind(&r)
	if err != nil {
		response.RespValidateError(validator.FormatValidatorError(err), gc)
		return
	}

	err = c.taskRepository.SwitchStatus(r.Ids, r.Status)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(nil, gc)
	return
}

func (c *task) ExecTimeList(gc *gin.Context) {
	idQueryRequest := request.GetIdQueryRequest(gc)
	if idQueryRequest == nil {
		response.RespError("id不能为空", gc)
		return
	}

	detail, err := c.taskRepository.ExecTimeList(idQueryRequest.Id)
	if err != nil {
		response.RespError(err.Error(), gc)
		return
	}
	response.RespSuccess(detail, gc)
	return
}
