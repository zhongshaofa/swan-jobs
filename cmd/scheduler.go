package main

import (
	"flag"
	"fmt"
	"github.com/zhongshaofa/swan-jobs/configs"
	"github.com/zhongshaofa/swan-jobs/internal"
	"github.com/zhongshaofa/swan-jobs/internal/plugin"
	ps "github.com/zhongshaofa/swan-jobs/internal/rpc/server/scheduler"
	"github.com/zhongshaofa/swan-jobs/internal/services"
	"github.com/zhongshaofa/swan-jobs/internal/services/client_heartbeat"
	"github.com/zhongshaofa/swan-jobs/internal/utils"
)

var (
	TaskInitService *services.TaskInitService
)

func init() {
	confPath := flag.String("conf", "../conf", "conf 配置目录")
	flag.Parse()

	go internal.AppSignal()

	loadSchedulerConfig(confPath)
	pluginSchedulerSetup()
}

func main() {
	TaskInitService = services.NewTaskInitService()
	client_heartbeat.InitClientHeartbeat()

	go TaskInitService.InitTask()
	go client_heartbeat.CheckHeartbeat()
	go TaskInitService.ListenSupervisorClientStatus()

	rpcPort := fmt.Sprintf(":%d", configs.SchedulerConfig.RpcPort)
	utils.PrintSystemDescribe(utils.ServeNameScheduler, rpcPort)
	ps.Start(rpcPort)
}

func loadSchedulerConfig(confPath *string) {
	configs.LoadAppConfig(*confPath)
	configs.LoadDatabaseConfig()
	configs.LoadSchedulerConfig()
}

func pluginSchedulerSetup() {
	plugin.LoggerSetup()
	plugin.ModelSetup()
}
