package middleware

import (
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/zhongshaofa/swan-jobs/configs"
	"github.com/zhongshaofa/swan-jobs/internal/models"
	"github.com/zhongshaofa/swan-jobs/internal/request"
	"github.com/zhongshaofa/swan-jobs/internal/services/token"
	"github.com/zhongshaofa/swan-jobs/internal/utils/jaeger_trace"
	"github.com/zhongshaofa/swan-jobs/internal/utils/response"
	"go.uber.org/zap"
	"net"
	"net/http"
	"net/http/httputil"
	"os"
	"runtime/debug"
	"strings"
	"time"
)

// GinLogger 接收gin框架默认的日志
func GinLogger(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		query := c.Request.URL.RawQuery
		c.Next()

		cost := time.Since(start)
		logger.Info(path,
			zap.Int("status", c.Writer.Status()),
			zap.String("method", c.Request.Method),
			zap.String("path", path),
			zap.String("query", query),
			zap.String("ip", c.ClientIP()),
			zap.String("user-agent", c.Request.UserAgent()),
			zap.String("errors", c.Errors.ByType(gin.ErrorTypePrivate).String()),
			zap.Duration("cost", cost),
		)
	}
}

// GinRecovery recover掉项目可能出现的panic
func GinRecovery(logger *zap.Logger, stack bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// Check for a broken connection, as it is not really a
				// condition that warrants a panic stack trace.
				var brokenPipe bool
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") || strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}

				httpRequest, _ := httputil.DumpRequest(c.Request, false)
				if brokenPipe {
					logger.Error(c.Request.URL.Path,
						zap.Any("error", err),
						zap.String("request", string(httpRequest)),
					)
					// If the connection is dead, we can't write a status to it.
					c.Error(err.(error)) // nolint: errcheck
					c.Abort()
					return
				}

				if stack {
					logger.Error("[Recovery from panic]",
						zap.Any("error", err),
						zap.String("request", string(httpRequest)),
						zap.String("stack", string(debug.Stack())),
					)
				} else {
					logger.Error("[Recovery from panic]",
						zap.Any("error", err),
						zap.String("request", string(httpRequest)),
					)
				}
				c.AbortWithStatus(http.StatusInternalServerError)
			}
		}()
		c.Next()
	}
}

// VerifyAuth 验证用户登录权限
func VerifyAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.GetHeader("token")
		if len(tokenString) == 0 {
			response.RespOtherError(response.CodeNoLoginError, "用户暂未登录", c)
			c.Abort()
			return
		}

		err, t := token.GetUserTokenMangerInstance().Verify(tokenString)
		if err != nil {
			response.RespOtherError(response.CodeNoLoginError, err.Error(), c)
			c.Abort()
			return
		}

		c.Set(models.CtxUserId, t.UserId)
		c.Set(models.CtxRoleType, t.RoleType)

		c.Next()

	}
}

// VerifyAllow 验证是否有管理员权限
func VerifyAllow() gin.HandlerFunc {
	return func(c *gin.Context) {
		authRequest, err := request.GetAuthRequest(c)
		if err != nil {
			response.RespOtherError(response.CodeNoLoginError, "用户暂未登录", c)
			c.Abort()
			return
		}
		if authRequest.RoleType != models.RoleTypeAdmin {
			response.RespOtherError(response.CodeNoAllowError, "用户没有权限", c)
			c.Abort()
			return
		}

		c.Next()

	}
}

// JaegerTrace 全链路日志
func JaegerTrace() gin.HandlerFunc {
	return func(c *gin.Context) {
		if configs.AppConfig.JaegerConfig.Enable {
			var parentSpan opentracing.Span

			tracer, closer := jaeger_trace.NewJaegerTracer(
				configs.AppConfig.JaegerConfig.ServiceName,
				configs.AppConfig.JaegerConfig.HostPort,
			)
			defer closer.Close()

			spCtx, err := opentracing.GlobalTracer().Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(c.Request.Header))
			if err != nil {
				parentSpan = tracer.StartSpan(c.Request.URL.Path)
				defer parentSpan.Finish()
			} else {
				parentSpan = opentracing.StartSpan(
					c.Request.URL.Path,
					opentracing.ChildOf(spCtx),
					opentracing.Tag{Key: string(ext.Component), Value: "HTTP"},
					ext.SpanKindRPCServer,
				)
				defer parentSpan.Finish()
			}
			c.Set("Tracer", tracer)
			c.Set("ParentSpanContext", parentSpan.Context())
		}
		c.Next()
	}
}
