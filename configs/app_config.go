package configs

import (
	"fmt"
	"gopkg.in/ini.v1"
	"os"
)

var AppConfig *app

// app 应用配置
type app struct {
	ConfPath     string
	Debug        bool
	LoggerConfig *loggerConfig
	JaegerConfig *jaegerConfig
}

// loggerConfig 日志配置
type loggerConfig struct {
	LogPath string
	LogName string
}

type jaegerConfig struct {
	Enable      bool
	ServiceName string
	HostPort    string
}

// LoadAppConfig 加载APP应用配置
func LoadAppConfig(confPath string) {
	cfg, err := ini.Load(confPath + "/app.ini")
	if err != nil {
		fmt.Printf("Fail to read file app.ini: %v", err)
		os.Exit(1)
	}

	debug, _ := cfg.Section("app").Key("debug").Bool()

	logPath := cfg.Section("logger").Key("log_path").String()
	logName := cfg.Section("logger").Key("log_name").String()

	enable, _ := cfg.Section("jaeger").Key("enable").Bool()
	serviceName := cfg.Section("jaeger").Key("service_name").String()
	hostPort := cfg.Section("jaeger").Key("host_port").String()

	AppConfig = &app{
		ConfPath: confPath,
		Debug:    debug,
		LoggerConfig: &loggerConfig{
			LogPath: logPath,
			LogName: logName,
		},
		JaegerConfig: &jaegerConfig{
			Enable:      enable,
			ServiceName: serviceName,
			HostPort:    hostPort,
		},
	}
}
