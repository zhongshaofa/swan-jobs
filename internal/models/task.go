package models

import (
	"database/sql"
	"github.com/zhongshaofa/swan-jobs/configs"
	"strconv"
)

const (
	ModeSupervisor = 1
	ModeCron       = 2
)

const (
	ScheduleTypeOne         = 1
	ScheduleTypeIdle        = 2
	ScheduleTypeDistributed = 3
)

type Task struct {
	Base
	AppId        int            `gorm:"column:app_id;default:0;NOT NULL" json:"app_id"`
	Name         string         `gorm:"column:name;NOT NULL" json:"name"`                             // 任务名称
	Manager      string         `gorm:"column:manager;NOT NULL" json:"manager"`                       // 负责人
	Mode         int            `gorm:"column:mode;default:1;NOT NULL" json:"mode"`                   // 任务类型 1=守护进程 2=定时执行
	CronTimeOut  int            `gorm:"column:cron_time_out;default:6;NOT NULL" json:"cron_time_out"` // 仅针对 任务类型=定时执行
	CronFormula  string         `gorm:"column:cron_formula;NOT NULL" json:"cron_formula"`             // 定时任务表达式
	ScheduleType int            `gorm:"column:schedule_type;default:1;NOT NULL" json:"schedule_type"` // 调度类型 1=单机运行 2=空闲执行 3=分布式
	ExecuteType  int            `gorm:"column:execute_type;default:1;NOT NULL" json:"execute_type"`   // 执行类型 1=直接执行 2=shell命令(无法守护进程)
	Directory    string         `gorm:"column:directory;NOT NULL" json:"directory"`                   // 执行目录
	Command      string         `gorm:"column:command;NOT NULL" json:"command"`                       // 执行命令
	ShellSource  sql.NullString `gorm:"column:shell_source" json:"shell_source"`                      // shell命令源码
	NotifyId     int            `gorm:"column:notify_id;default:0;NOT NULL" json:"notify_id"`         // 通知
	Remark       string         `gorm:"column:remark;NOT NULL" json:"remark"`
	Status       int            `gorm:"column:status;default:0;NOT NULL" json:"status"` // 调度状态：2-停止，1-运行
	CreateUid    int            `gorm:"column:create_uid;default:0;NOT NULL" json:"create_uid"`
}

func (m *Task) TableName() string {
	return configs.DatabaseConfig.Prefix + "task"
}

func (m *Task) CrontabName() string {
	return "crontab-" + strconv.Itoa(int(m.ID))
}
