package test

import (
	"fmt"
	"github.com/zhongshaofa/swan-jobs/internal/utils"
	"testing"
)

func TestHttpGet(t *testing.T) {
	url := "https://www111.baidu.com"
	err, s := utils.HttpGet(url)
	fmt.Println(err)
	fmt.Println(s)
}
