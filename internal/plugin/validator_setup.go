package plugin

import (
	"github.com/go-playground/locales/en"
	"github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	zhTranslations "github.com/go-playground/validator/v10/translations/zh"
)

var ValidatorTranslator ut.Translator
var Validate *validator.Validate

func ValidatorSetup() {
	Validate = validator.New()
	english := en.New()
	chinese := zh.New()
	uni := ut.New(chinese, english)
	ValidatorTranslator, _ = uni.GetTranslator("zh")
	_ = zhTranslations.RegisterDefaultTranslations(Validate, ValidatorTranslator)
}
