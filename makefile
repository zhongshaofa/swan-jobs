GO111MODULE=on

# 构建服务并启动
.PHONY: run
run: build serve

.PHONY: backend
backend: SwanBackend serve_backend

.PHONY: client
client: SwanClient serve_client

.PHONY: scheduler
scheduler: SwanScheduler serve_scheduler

# 启动服务
.PHONY: serve
serve:
	./build/SwanClient -conf=deployments/conf
	./build/SwanScheduler -conf=deployments/conf
	./build/SwanBackend -conf=deployments/conf

.PHONY: serve_backend
serve_backend:
	./build/SwanBackend -conf=deployments/conf

.PHONY: serve_client
serve_client:
	rm -rf /tmp/scripts
	cp -r scripts /tmp/scripts
	./build/SwanClient -conf=deployments/conf

.PHONY: serve_scheduler
serve_scheduler:
	./build/SwanScheduler -conf=deployments/conf

# 构建服务
.PHONY: build
build:SwanClient SwanScheduler SwanBackend

# 构建服务
.PHONY: build-linux
build-linux:SwanBackend-linux

# 清理可执行文件
.PHONY: clean
clean:
	rm build/SwanClient
	rm build/SwanScheduler
	rm build/SwanBackend

# 杀掉可执行进程
.PHONY: kill
kill:
	-killall SwanBackend
	-killall SwanClient
	-killall SwanScheduler


.PHONY: kill_backend
kill_backend:
	-killall SwanBackend

.PHONY: kill_client
kill_client:
	-killall SwanClient

.PHONY: kill_scheduler
kill_scheduler:
	-killall SwanScheduler

# 构建SwanClient
.PHONY: SwanClient
SwanClient:
	go build -o build/SwanClient ./cmd/client.go

# 构建SwanScheduler
.PHONY: SwanScheduler
SwanScheduler:
	go build -o build/SwanScheduler ./cmd/scheduler.go

# SwanBackend
.PHONY: SwanBackend
SwanBackend:
	go build -o build/SwanBackend ./cmd/backend.go

# 构建SwanClient-linux
.PHONY: SwanClient-linux
SwanClient-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o build/SwanClient-linux ./cmd/client.go

# 构建SwanScheduler-linux
.PHONY: SwanScheduler-linux
SwanScheduler-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o build/SwanScheduler-linux ./cmd/scheduler.go

# SwanBackend-linux
.PHONY: SwanBackend-linux
SwanBackend-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o build/SwanBackend-linux ./cmd/backend.go

# go project test
.PHONY: test
test:
	go test ./test/ -v


