package test

import (
	"fmt"
	"github.com/jakecoffman/cron"
	"github.com/zhongshaofa/swan-jobs/test/tool"
	"testing"
	"time"
)

func TestCron(t *testing.T) {
	tool.InitDB()
	i := 0
	c := cron.New()
	c.AddFunc("0/5 * * * * ?", func() {
		fmt.Printf("TestCron AddFunc running:%v", i)

		if i == 5 {
			c.RemoveJob("test")
		}

		i++
	},
		"test")
	c.Start()

	list := c.Entries()

	for _, value := range list {
		fmt.Printf("value = %+v\n", value)
	}

	select {}
}

func TestParser(t *testing.T) {
	//Standard parser without descriptors
	schedule := cron.Parse("0/5 * * * * ?")

	nowTime := time.Now()
	next1 := schedule.Next(nowTime)
	next2 := schedule.Next(next1)
	next3 := schedule.Next(next2)

	fmt.Println(nowTime, next1, next2, next3)
}
